package main

import (
	"fmt"
	"strings"
	"../inventoryservice"
)

func InvokeInventorySerivce(){

	var api string
	fmt.Println("Enter the api to be tested in Inventory Service :\n   " +
		"Sell Inventory API --> 1\n   " +
		"Cancel Sold Inventory API--> 2\n   " +
		"Onhold Inventory --> 3\n   " +
		"Block Inventory --> 4\n   " +
		"Block to sell -->5\n   " +
		"Block to onhold --> 6")
	fmt.Scanln(&api)

	//Sell or cancel inventory
	if api == "1" || api == "2" || api == "3" || api == "4"{
		var numberOfRequests int
		var numberOfConcurrentRequests uint
		var flightNumbers string
		var bookingClass string
		var childCapacities string
		var infantCapacities string
		var adultCapacities string
		var departureDateLocal string

		fmt.Println("Enter the number of requests to be sent : ")
		fmt.Scanln(&numberOfRequests)

		fmt.Println("Enter the number of concurrent requests to be sent : ")
		fmt.Scanln(&numberOfConcurrentRequests)

		fmt.Println("Enter flight number separated by comma : ")
		fmt.Scanln(&flightNumbers)

		fmt.Println("Enter booking class : ")
		fmt.Scanln(&bookingClass)

		fmt.Println("Enter child capacity separated by comma : ")
		fmt.Scanln(&childCapacities)

		fmt.Println("Enter infant capacity separated by comma : ")
		fmt.Scanln(&infantCapacities)

		fmt.Println("Enter adult capacity separated by comma : ")
		fmt.Scanln(&adultCapacities)


		fmt.Println("Enter departure date time local (format => yyyy-MM-dd'T'HH:mm:ss) :")
		fmt.Scanln(&departureDateLocal)

		if api == "1" {
			inventoryservice.SellInventory(numberOfRequests,
				numberOfConcurrentRequests,
				strings.Split(flightNumbers, ","),
				strings.Split(childCapacities, ","),
				strings.Split(infantCapacities, ","),
				strings.Split(adultCapacities, ","),
				bookingClass,
				departureDateLocal)
		}else if api == "2"{
			inventoryservice.CancelSoldInventory(numberOfRequests,
				numberOfConcurrentRequests,
				strings.Split(flightNumbers, ","),
				strings.Split(childCapacities, ","),
				strings.Split(infantCapacities, ","),
				strings.Split(adultCapacities, ","),
				bookingClass,
				departureDateLocal)
		}else if api == "3"{
			inventoryservice.OnholdInventory(numberOfRequests,
				numberOfConcurrentRequests,
				strings.Split(flightNumbers, ","),
				strings.Split(childCapacities, ","),
				strings.Split(infantCapacities, ","),
				strings.Split(adultCapacities, ","),
				bookingClass,
				departureDateLocal)
		}else if api == "4" {

			var timePeriod string
			fmt.Println("Enter blocked time period")
			fmt.Scanln(&timePeriod)

			inventoryservice.BlockInventory(numberOfRequests,
				numberOfConcurrentRequests,
				strings.Split(flightNumbers, ","),
				strings.Split(childCapacities, ","),
				strings.Split(infantCapacities, ","),
				strings.Split(adultCapacities, ","),
				bookingClass,
				departureDateLocal,
				timePeriod)
		}

	}else if api == "5" || api == "6"{

		var numberOfRequests int
		var numberOfConcurrentRequests uint
		fmt.Println("Enter the number of requests to be sent : ")
		fmt.Scanln(&numberOfRequests)

		fmt.Println("Enter the number of concurrent requests to be sent : ")
		fmt.Scanln(&numberOfConcurrentRequests)


		var blockIds string
		fmt.Println("Enter block ids separated by comma")
		fmt.Scanln(&blockIds)

		if api == "5" {
			inventoryservice.SellBlockedInventory(numberOfRequests,numberOfConcurrentRequests,strings.Split(blockIds,","))
		}else if api == "6"{
			inventoryservice.OnholdBlockedInventory(numberOfRequests,numberOfConcurrentRequests)
		}


	}
}
