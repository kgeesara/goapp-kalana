package main

import (
	"fmt"
	"../flightservice"
	"strings"
)

func InvokeFlightService(){

	//getFlightNumberList
	var api string
	fmt.Println("Enter the api to be tested in Flight Service : \n   " +
		"Get Flight Number API --> 1\n   " +
		"Close all flight API --> 2\n   " +
		"Save flight note API --> 3")
	fmt.Scanln(&api)

	if api == "1" || api == "2" || api == "3"{
		var numberOfRequest int
		var concurrency uint

		fmt.Println("Enter the number of requests to be sent : ")
		fmt.Scanln(&numberOfRequest)

		fmt.Println("Enter the number of concurrent requests to be sent : ")
		fmt.Scanln(&concurrency)

		if api == "1"{
			flightservice.GetFlightNumbersSendRequest(numberOfRequest,concurrency)
		}else if api == "2"{

			var dateRangeStartingDate string
			var dateRangeSize int

			fmt.Println("Enter the date range starting date : ")
			fmt.Scanln(&dateRangeStartingDate)

			fmt.Println("Enter the date range size : ")
			fmt.Scanln(&dateRangeSize)


			flightservice.GetCloseAllFlightsRQ(numberOfRequest,concurrency,dateRangeStartingDate, dateRangeSize)

		}else if api == "3"{
			var flightId string
			var text string
			var applyForTargetFlight string

			fmt.Println("Enter the source flight Id separated by comma : ")
			fmt.Scanln(&flightId)

			fmt.Println("Enter the note to be saved separated comma : ")
			fmt.Scanln(&text)

			fmt.Println("Enter the input for apply for target flight [true/false] separated by comma : ")
			fmt.Scanln(&applyForTargetFlight)
			flightservice.SaveFlightNote(numberOfRequest,concurrency,
				strings.Split(flightId,","),
				strings.Split(text,","),
				strings.Split(applyForTargetFlight,","))

		}

	}

}
