package main

import (
	"fmt"
)

func main() {

	var service string
	fmt.Println("Enter the service to be tested: \n   " +
		"Flight Service --> 1 \n   " +
		"Inventory Service --> 2\n   " +
		"Roll Forward Service --> 3\n   " +
		"Flight Inventory Service --> 4\n   " +
		"Roll Forward->Block->On Hold->Sell->Cancel full flow --> 5\n   " +
		"Clean up database --> 6")

	fmt.Scanln(&service)

	if service == "1" {
		InvokeFlightService()

	}else if service == "2" {
		InvokeInventorySerivce()

	}else if service == "3" {
		InvokeRollForwardService()

	}else if service== "4" {
		InvokeFlightInventoryService()

	}else if service == "5"{
		InvokeRollFwdToCancelFlow()

	}else if service == "6"{
		InvokeCleanUpDatabase()
	}

}