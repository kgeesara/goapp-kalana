package flightinventoryservice

import (
	aeroinventory "com/accelaero/aeroinventory"
	"fmt"
	"github.com/bojand/ghz/printer"
	"github.com/bojand/ghz/runner"
	"github.com/golang/protobuf/proto"
	"github.com/jhump/protoreflect/desc"
	"os"
	"strconv"
)

func getSearchFlightInventoryDataFunc(mtd *desc.MethodDescriptor, cd *runner.CallData) []byte{

	res := cd.RequestNumber % int64(len(flightIdG))
	flightInventorySearchRQ := &aeroinventory.FlightInventorySearchRQ{}
	fi, err := strconv.ParseInt(flightIdG[res], 10, 32)
	flightInventorySearchRQ.FlightId = int32(fi)
	binData, err := proto.Marshal(flightInventorySearchRQ)

	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
	return binData

}

func SearchFlightInventory(numberOfRequests int, numberOfConcurrentRequests uint, flightId []string) {

	flightIdG = flightId

	report, err := runner.Run(
		"aeroinventory.FlightInventoryService.searchFlightInventory",
		"localhost:6565",
		runner.WithProtoFile("/home/chethan/Codebase/goapp-local/goProtos/proto/flight_inventory_allocation.proto", []string{}),
		runner.WithInsecure(true),
		runner.WithConcurrency(numberOfConcurrentRequests),
		runner.WithTotalRequests(uint(numberOfRequests)),
		runner.WithBinaryDataFunc(getSearchFlightInventoryDataFunc),
	)

	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}

	printer := printer.ReportPrinter{
		Out:    os.Stdout,
		Report: report,
	}
	printer.Print("pretty")
}

