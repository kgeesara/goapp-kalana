package databasecalls

import (
	"encoding/json"
	"os"
	"../structs"
)

func LoadConfiguration(filename string)(structs.Config, error){
	var config structs.Config
	configFile, err := os.Open(filename)
	defer configFile.Close()
	if err != nil{
		return config, err
	}
	jsonParser := json.NewDecoder(configFile)
	err = jsonParser.Decode(&config)

	return config, err
}